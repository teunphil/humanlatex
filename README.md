# HumanLaTeX :dancer:

**Neovim plus LaTeX for the humanities and social sciences.**

This is for all those LaTeX users in the humanities and social sciences just trying to figure stuff out.
I am one of them.
Here I share what I've learned.

![Example of a Neovim+LaTeX workflow](https://gitlab.com/teunphil/humanlatex/-/raw/main/example.gif)*What my Neovim+LaTeX workflow looks like.*

---

## Contents
1. [Introduction](#introduction)

2. [Features](#features)

3. [Reasons for using LaTeX](#reasons-for-using-latex)

4. [Reasons for using Neovim](#reasons-for-using-neovim)

5. [Getting started with Neovim](#getting-started-with-neovim)

6. [Neovim plugins](#neovim-plugins)

7. [Neovim tips & tricks](#neovim-tips-tricks)

8. [Getting started with LaTeX](#getting-started-with-latex)

9. [Useful LaTeX packages](#useful-latex-packages)

10. [LaTeX tips & tricks](#latex-tips-tricks)

11. [Further resources](#further-resources)

---

## Introduction

I've spent a good deal of time figuring stuff out when it comes to writing philosophy papers in LaTeX with Vim, and later Neovim.
Although I'm by no means an expert (I still [Duck](https://duckduckgo.com/) stuff on a daily basis), I have accumulated a fair bit of knowledge on how to comfortably write pretty documents in a Neovim+LaTeX setup.
Here I share my setup block by block.
I don't actually recommend copying my entire setup (why would you? you're not me), but rather to follow along and see what works for you and what doesn't (or, of course, to cherry-pick solutions to specific problems you're having).

Also, if you're like "Hey, I'm not a programmer, I just want to think about Dasein instead of using *code* to, like, *program* my *editor* or whatever", that's totally valid!
You can just use the LaTeX stuff and ignore the Neovim stuff.
Or, of course, you could turn back to MS Word altogether and occasionally, when inserting an image has once again brought your entire system to a screeching halt, you look out the window and think about the beautiful documents you could be writing if you had just followed that random internet person's guide.
But you know, no judgement.

## Features
My setup features the following:

- On the editor side:
	- Automatic and continuous compiling
	- Pretty great syntax highlighting
	- LaTeX-specific motions and commands
	- LanguageTool grammar checking
	- LaTeX diagnostics
	- Pretty colours

- On the output side:
	- Simple and elegant documents
	- Clickable links everywhere
	- Automatic references and bibliography
	- Support for multiple languages
	- Two-column support
	- Abstract and keywords
	- Subtitles, sub-headers, sub-everything
	- Tables, images, and appendices if you're into them

Features it **doesn't** have, but probably should:

- Persistent word count in the statusline (tried it with `vimtex#misc#wordcount()`, but I'm too stupid to make it work)
- An easy way to add exceptions to LTeX spellchecking

Features it **doesn't** have, and probably never will (because they don't cover my use case):

- Snippets
- Autocompletion
- File manager and related functions
- Extensive support for working on multiple files simultaneously
- A fully fledged IDE-like environment à la [SpaceVim](https://github.com/SpaceVim/SpaceVim) or [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)

## Reasons for using LaTeX

LaTeX is different from your average word processor such as Word or Writer.
We call these [What You See Is What You Get (WYSIWYG)](https://en.wikipedia.org/wiki/WYSIWYG) editors, because, well, they let you visually edit your documents in real-time.
In contrast, LaTeX is a *language* in which you can specify the content and form of your document.
This has a few advantages:

### Precision and consistency
In LaTeX, what happens to your document is only what you tell it to.
This means that Word annoyances such as random typographic changes or that infuriating vaguely blue background behind pasted text are unthinkable.
LaTeX feels very robust, because you first specify what you want your document to look like (in the 'preamble'), and then you type out your document which will be formatted exactly how you specified it.
This is what people mean when they say that LaTeX "separates form from content".
I'm personally a big fan, because when I'm writing I don't care where exactly the line breaks or how this figure ends up on the page or how that footnote looks.
With LaTeX, I can trust that these things will sort themselves out, and I can focus on writing instead.

In Word or Writer, formatting information is often 'hidden': "why is this paragraph all underlined in red?
Ah, it's because the program thinks it's in a different language for some reason."
In LaTeX, every change, whether local or global, is declared explicitly.
So instead of applying a hidden option to a certain chunk of text, you write it out:

```latex
As Descartes said: ``\foreignlanguage{latin}{cogito, ergo sum}''.
```

The same goes for bold and italics, font size, spacing, colour, links, capitalisation, footnotes, all the way up to tables and bibliographies: everything is explicitly declared.
This makes unpleasant surprises almost impossible.

### Ａ Ｅ Ｓ Ｔ Ｈ Ｅ Ｔ Ｉ Ｃ Ｓ
If you make a new document in Word or Writer, use all the defaults and write a few sentences with a header, the result is… not dashing.
If you make a new LaTeX document and add the bare minimum, the result looks awesome and professional.
Beautiful, fully justified text in a classy font and with lovely titles and headers (and no blue!).
And with some small tweaks you can make it look even better!

### Referencing
In my experience, referencing to sources in Word is okay, but it feels a bit like a hack.
By contrast, referencing to sources in LaTeX is buttery smooth.
You tell it to use a certain style and all formatting happens automatically, including in the bibliography.
The bibliography itself can be called with a single command and is automatically populated with all referenced sources.

### Version control
`thesis.docx` `thesisfinal.docx` `thesisfinalFINAL.docx` `thesisREALLYFINALFORREAL.docx` `final_thesisREALLYFINALFORREAL.docx`

LaTeX allows you to completely avoid endless versions of the same document and streamline the version control process.
This is because LaTeX files are just plaintext files, and plaintext files are manageable using `git`.
If you've never used Git, it is another one of these Spartan-looking programs that is somewhat difficult to get into.
But the basics are easy to learn, and there are plenty of [beginners' guides](https://towardsdatascience.com/an-easy-beginners-guide-to-git-2d5a99682a4c).

If you use Git consistently, you'll have a snapshot of every previous state of your document which you can revert to if necessary.
It also allows you to keep track of different 'branches' (for example, for teachers' suggestions) which you can later merge into your main document.
And finally, Git allows for very smooth collaboration in groups.

## Reasons for using Neovim

Neovim is not the most straightforward text editor to work with.
If you're used to what-you-see-is-what-you-get word processors like Word or Writer, the switch to Neovim+LaTeX will cause double whiplash; one for each part of your new setup.
So if that's your background, Neovim might not be the obvious choice, and you might actually want to opt for a more 'conventional' editor such as [Overleaf](https://www.overleaf.com/) (online LaTeX editor), [TeXstudio](https://www.texstudio.org/) (offline LaTeX editor) or even a more conventional generic text editor such as [Emacs](https://tex.stackexchange.com/questions/339/latex-editors-ides/356#356).
(See [this Reddit thread](https://old.reddit.com/r/LaTeX/comments/kaqkhq/whats_a_good_latex_editor_for_a_beginner/) and [this Stackexchange thread](https://tex.stackexchange.com/questions/339/latex-editors-ides/) for more inspiration.)
However, there are reasons for opting for Neovim, despite its rather steep learning curve.

### Efficiency
Vim and Neovim are often touted for being extremely efficient once you know how to work with it.
This is mostly because of its modular nature.
That is: it has different modes of operation where, depending on the mode, keys mean different things.

You start out in 'normal mode', which is an editing efficiency monster.
Every key on your keyboard has an editing purpose in normal mode, which allows you to do almost anything you want in a few keystrokes.
For example, you can delete a line with `dd`.
Deleting three words? `d3w`.
Deleting everything until that closing parenthesis? `df)`
The same goes for moving around.
`50gg` brings you to line 50.
`}` takes you to the next empty line.
`3w` jumps you three words ahead.

This can be extended with LaTeX-specific bindings such as `]]` for jumping to the next section, or `cse` to change the surrounding environment (see the [VimTeX](#vimtex) section).

Because the keyboard is so powerful in normal mode, you don't have to rely on your mouse at all for moving and editing.
This is, for many people, a very pleasant experience once they learn the basics.

It is also these different modes that can make Vim [so insanely complicated](https://rawgit.com/darcyparker/1886716/raw/eab57dfe784f016085251771d65a75a471ca22d4/vimModeStateDiagram.svg).
But if you take it slow and learn only what is most relevant to you (that's what I'm doing), they can be crazy powerful.

### Lightness and speed
Neovim is incredibly light and fast by default.
Even when loaded with plugins, it never has any noticeable input delay for me, which is great for these flowy moments where you just want to type.

### Control and openness
Neovim is [open-source](https://github.com/neovim/neovim) and gives the user a **lot** of control.
I want a tool that I control, not one that controls me (and especially not one developed by a company that makes money by [selling my data](https://en.wikipedia.org/wiki/Microsoft_Advertising) to make me buy more stuff I don't need).

### Customisability and extendability
Neovim can be customised to be exactly the way you like, and it doesn't come with a ton of superfluous features by default.
For example, you can define your own keybindings for common tasks, or thoroughly change Neovim's looks and behaviour.
It can also be extended with endless plugins.
For LaTeX writing, [I recommend a few below](#2-nvim-plugins)!

### Vim versus Neovim
This knowledgebase was recently converted from being Vim/Neovim-agnostic to fully Neovim-dependent.
(See the old repo [here](https://gitlab.com/teunphil/sophia-template) if you're looking for some info on good ol' Vim.)
The debate on Vim versus Neovim is enormous and not worth getting into here.
For my own purposes, I found that Neovim's plugin ecosystem and its native LSP support made it the more obvious choice.
For casual users, there are actually very few glaring differences between the two.
Neovim is seen by many (but not all!) as the future of Vim development, and it does indeed seem to have a very vibrant community (see [This Week In Neovim](https://this-week-in-neovim.org/) to get an idea).
However, some say that it moves too fast, and they might prefer the more conservative approach of Vim.

## Getting started with Neovim

I highly recommend following along with [John McBride's Neovim setup video](https://youtu.be/-esgEOqwzVg).

That said, the basics are as follows.
You'll want to create a Neovim config folder (`~/.config/nvim` on Linux), and give it the following structure:

```
<nvim-config-folder>
├── init.lua
├── lua
│   ├── keymaps.lua
│   └── plugins.lua
└── plugin
    ├── plugin1.lua
    ├── plugin2.lua
    └── etc.
```

`init.lua` is your main configuration file.
Neovim will do everything you put in that file, everytime it opens.
I like to put only options in there, and put my keymaps and plugins in separate files `keymaps.lua` and `plugins.lua` respectively.
By putting these files in a subdirectory called `lua`, Neovim will already know where to look, and all you need to add to `init.lua` is the following:

```lua
----<nvim-config-folder>/init.lua
-- Plugins --------------------------------------------------------------
require('plugins')

-- Keymaps --------------------------------------------------------------
require('keymaps')
```

I like to put these at the bottom of the config file so that they load after the options, but you do you.

**A simple Neovim config**

Neovim requires little extra config for comfy LaTeX editing, so the settings here are quite vanilla.

```lua
----<nvim-config-folder>/init.lua
vim.opt.number = true		-- show number line
vim.opt.ignorecase = true	-- ignore case when searching
vim.opt.smartcase = true	-- ignore case only with lowercase letters
vim.opt.wrap = true		-- enable soft wrapping at the edge of the screen
vim.opt.linebreak = true 	-- don't wrap in the middle of a word
vim.opt.mouse = "a"		-- enable mouse in all modes

```

## Neovim plugins

### [Packer](https://github.com/wbthomason/packer.nvim)
You can manage your plugins [manually](https://gist.github.com/manasthakur/ab4cf8d32a28ea38271ac0d07373bb53#managing-plugins-natively-using-vim-8-packages), but for me, there's little reason not to use a plugin like Packer.
It's incredibly simple, it supports anything that can be found on GitHub, and it can manage itself!
Install it by following the [quickstart instructions](https://github.com/wbthomason/packer.nvim#quickstart). 

I put all Packer-related configuration in `<nvim-config-folder>/lua/plugins.lua` and then `require` the file in `init.lua` (see [getting started](#getting-started-with-neovim)).

Install newly added plugins and update existing ones with `:PackerSync`.

### [VimTeX](https://github.com/lervag/vimtex)

If you're going to install just one LaTeX-related plugin, make it this one.
VimTeX is a very complete filetype plugin for LaTeX files.
It has many config options, but I have only set the following:

```lua
----<nvim-config-folder>/plugin/vimtex.lua
vim.g.vimtex_view_method = 'zathura'	-- set VimTeX default pdf viewer
vim.g.vimtex_fold_enabled = 1 		-- enable VimTeX folding

vim.opt.conceallevel = 2		-- set conceal level to 2 (see :h conceallevel)

vim.g.vimtex_syntax_conceal = {		-- enable or disable specific conceals
	accents = 1,
	ligatures = 1,
	cites = 1,
	fancy = 1,
	spacing = 0,			-- default: 1
	greek = 1,
	math_bounds = 1,
	math_delimiters = 1,
	math_fracs = 1,
	math_super_sub = 1,
	math_symbols = 1,
	sections = 0,
	styles = 1,
	}
```

#### Features

**Auto-compilation.**
Perhaps the most convenient feature is auto-compilation.
To enable it, simply open a LaTeX document and hit `\ll`.
Your document will be compiled and opened in the pdf reader set with `vim.g.vimtex_view_method`.

**→ NOTE:** `\` is the default 'leader key' on Neovim.
The leader key is meant to prevent custom keybindings from interfering with existing ones.
This is why a lot of plugins have keybindings that start with `<leader>`, which in practice means `\`.
To change it, for example to `,`, add this line to your Vimtex config: `vim.g.maplocalleader = ","`.
Going forward, I will assume `<leader>` is `\`.

**Forward search.**
Hit `\lv` in a LaTeX document to jump to the corresponding place in the pdf.

**Table of contents.**
Hit `\lt` to toggle a table of contents.

**Error window.**
Hit `\le` to toggle the quickfix window to check for compilation errors and warnings.

**Cleanup.**
Hit `\lc` to clean up auxilliary compilation files.

**Folding.**
Collapse certain parts of your document for a better overview.

- `za`	toggle current fold
- `zA`	toggle current fold + all underlying folds
- `zR`	open all folds ('**R**educe')
- `zM`	close all folds	('**M**ore')

**Auto-conceal.**
Makes your document more readable by concealing certain LaTeX functions such as citations, `\item` points, spacing commands and text styles.
You can set what exactly gets collapsed with `vim.g.vimtex_syntax_conceal` (See `:h g:vimtex_syntax_conceal` for more info).
I leave everything on default except `spacing`, because I find it annoying not to be able to see `\vspace{}` commands.

**Motions.**
Use `]]` and `[[` to quickly jump between sections.

**Command objects.**
- `ac`	A Command: the entire command from `\` to `}`
- `ic`	Inner Command: the text between brackets in the command

For example: hit `cic` while on a command to change the content of that command.
Or `dac` to delete an entire command.

**Environment objects.**

- `ae`	An Environment: the entire environment, from `\begin{}` to `\end{}`, including contents
- `ie`	Inner Environment: only the contents of the environment
- `se`	Surrounding Environment: only the environment commands


For example: hit `dae` to delete an entire environment.
Hit `cse` to change the environment type (this changes two lines simultaneously, how fancy!).

**Word count.**
`:VimtexCountWords` gives you an accurate (apart from the bibliography) word count for your final document.
You can set up a convenient keymap, such as:

```lua
----<nvim-config-folder>/lua/keymaps.lua
vim.api.nvim_set_keymap("n", "<localleader>lw", ":VimtexCountWords<CR>", { noremap = true }) -- Vimtex word count
```

Check the [VimTeX Github](https://github.com/lervag/vimtex#features) for a more complete list of features!

#### Changing TeX engines
If you want to use LuaLaTeX as a TeX engine instead of pdfLaTeX, you'll have to change this in the config for `latexmk`, which is the program that VimTeX invokes to compile documents.
On Linux, open `~/.config/latexmk/latexmkrc` (chances are you have to make the folder and file first), and add the following line ([source](https://github.com/mgeier/homepage/issues/7#issuecomment-930542435)):

```conf
$pdf_mode = 4; # LuaLaTeX
```

For XeLaTeX, add this ([source](https://tex.stackexchange.com/questions/27450/how-to-make-latexmk-work-with-xelatex-and-biber)):

```conf
$pdflatex="xelatex %O %S";
```

To check your current engine, use `:VimtexInfo`.

For the why of using a different engine, go [here](#pdflatex-vs-xelatex-vs-lualatex).

### [true-zen.nvim](https://github.com/Pocco81/true-zen.nvim)
This plugin adds a pleasant distraction-free writing mode.
I have it set up as follows.

Initializing the plugin in `plugin.lua`:

```lua
----<nvim-config-folder>/lua/plugin.lua
use 'pocco81/true-zen.nvim'		-- focused writing
```

And setting a keymap in `keymaps.lua`:

```lua
----<nvim-config-folder>/lua/keymaps.lua
vim.api.nvim_set_keymap("n", "<leader>za", ":TZAtaraxis<CR>", { noremap = true }) -- true-zen Ataraxis mode
```

This way, `\za` toggles true-zen in Ataraxis mode.

true-zen.nvim has [a ton of options](https://github.com/Pocco81/true-zen.nvim#%EF%B8%8F-configuration), including fancy Kitty (the terminal, not the fluffball) integration, but I find that I don't really need any of it.

### [Kanagawa](https://github.com/rebelot/kanagawa.nvim) (color scheme)
Do I really need to tell you what color scheme to use?
Anyhow, I'm currently on Kanagawa (although I do tend to hop frequently) because 1) I think it's beautiful, and 2) it gives me plenty of contrast, especially in plain text.

```lua
----<nvim-config-folder>/lua/plugins.lua
use 'rebelot/kanagawa.nvim'
```

Check [this page](https://github.com/rockerBOO/awesome-neovim/blob/main/README.md#colorscheme) for approximately one million other options.

*FYI: This color scheme stuff is the reason why this section is not called "**Useful** Neovim plugins" anymore.*

### LSP stuff
Oh sweey Mary, now we're getting into the weeds.
LSP (Language Server Protocol) is what allows you to have diagnostics directly in your buffer.
For my setup, that means two things: 1) language-related suggestions via LanguageTool, and 2) LaTeX diagnostics via Texlab.
LSP itself is built into Neovim, but in order to leverage it, you need plugins (at least I do).

Trying to keep things as simple as possible, I use the following plugins for setting up LSP:

- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) – quickstart configs for talking to language servers so you don't have to deal with them manually
- [Mason](https://github.com/williamboman/mason.nvim) – a package manager for langauge servers so you don't have to hunt them down and install them manually
- [Mason-lspconfig](https://github.com/williamboman/mason-lspconfig.nvim) – a bridge between the previous two so you don't have to make them work together manually

Install them like this, in this exact order (says who? [the plugin devs](https://github.com/williamboman/mason-lspconfig.nvim#setup)):

```lua
----<nvim-config-folder>/lua/plugins.lua
use {
	'williamboman/mason.nvim',
	'williamboman/mason-lspconfig.nvim',
	'neovim/nvim-lspconfig',
}
```

After installing, make the file `mason.lua` (or whatever you want to name it, I'm not your mom) in the `lua` folder and initialize `mason` and `mason-lspconfig` in it. (We'll initialize nvim-lspconfig itself server-by-server, a few steps later.)

```lua
----<nvim-config-folder>/plugin/mason.lua
require'mason'.setup{}
require'mason-lspconfig'.setup{}
```

Afterwards, the basic idea is that you 1) install language servers for the languages you need (probably LaTeX) through Mason, and 2) initialize them with lspconfig.

Luckily, Mason is incredibly easy to use.
Just type `:Mason` and you get a lovely interface where you can browse language servers and install them with `i`.
For LaTeX, I installed [ltex-ls](https://github.com/valentjn/ltex-ls) (LanguageTool for LaTeX) and [texlab](https://github.com/latex-lsp/texlab) (LaTeX diagnostics).

Next, initialize the language servers.
I do this in the same `mason.lua` file, but you can make a separate file if you wish.

```lua
----<nvim-config-folder>/plugin/mason.lua
require'lspconfig'.ltex.setup{}
require'lspconfig'.texlab.setup{}
```

There are settings available for both [ltex](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#ltex) and [texlab](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#texlab), but I don't really need them, so I just leave it at this.

Next, update your plugins with `:PackerSync`, and you should be good to go!

**→ NOTE:** If you want LTeX to pick up on your document-specific language settings, you have to pass a language argument to babel specifically, and not just to `\documentclass{}`.

At any time, you can check what language servers are installed and active in the current buffer with `:LspInfo`.

### [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
Let's keep things simple: I use Treesitter because it offers superior syntax highlighting compared to what Vimtex offers. ([See this comparison.](https://imgur.com/a/onvMh1r))

Treesitter functionality is kinda, maybe, [somewhat](https://neovim.io/doc/user/treesitter.html) built into Neovim, but we kinda, maybe, somewhat still need a plugin to use it: [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter).

Install it as follows:

```lua
----<nvim-config-folder>/lua/plugins.lua
use {
	'nvim-treesitter/nvim-treesitter',
	-- make sure that nvim-treesitter updates its installed parsers (see https://github.com/nvim-treesitter/nvim-treesitter#installation)
	run = ':TSUpdate'
}
```

Then initialize it:

```lua
----<nvim-config-folder>/plugin/nvim-treesitter.lua
require'nvim-treesitter.configs'.setup {
	highlight = {
		-- `false` will disable the whole extension
		enable = true,
		additional_vim_regex_highlighting = true, -- default: false
	},
}
```

I enable `additional_vim_regex_highlighting` because it simply gives me the best results with LaTeX files.

Note that I've only enabled the `highlight` module.
The plugin offers [other modules](https://github.com/nvim-treesitter/nvim-treesitter#available-modules) too, but I don't need them, so I don't enable them to keep life easy.

## Neovim tips & tricks

### System clipboard
In plain Neovim, copying (`y`), cutting (`d`) and pasting (`p`) happens through a Neovim-specific buffer.
Using the system clipboard requires you to append your action with `"+` (which basically means 'system (`+`) buffer (`"`) please').
If you don't want to deal with that, you can make Neovim always use the system clipboard by adding the following line to your config file:

```lua
vim.opt.clipboard = "unnamedplus"	-- set clipboard register to the system clipboard
```

### Cursorline
I like having a highlighted cursorline (`cul`) to show me what line I'm on, but not while I'm typing.
Enter this lovely little autocommand (add it to your config file, you know the deal):

```lua
----<nvim-config-folder>/init.lua
vim.api.nvim_create_autocmd({ "VimEnter" }, { pattern = "*", command = "set cul" }) -- add cursorline when not in insert mode
vim.api.nvim_create_autocmd({ "InsertLeave" }, { pattern = "*", command = "set cul" })
vim.api.nvim_create_autocmd({ "InsertEnter" }, { pattern = "*", command = "set nocul" })
```

### Spellcheck

**→ NOTE:** this is not really necessary if you already use the LTeX language server (see the section on [LSP](#LSP)).

To enable basic spellchecking, install `hunspell` + all relevant Hunspell dictionaries to your system.
I added the following to my config (in a separate `keymaps.lua` file, but you do you):

```lua
----<nvim-config-folder>/lua/keymaps.lua
vim.api.nvim_set_keymap("", "<F6>", ":setlocal spell! spelllang=en_UK<CR>", { noremap = true })	-- toggle UK_EN spellcheck
vim.api.nvim_set_keymap("", "<F7>", ":setlocal spell! spelllang=nl_NL<CR>", { noremap = true })	-- toggle NL spellcheck
```

This will give you a hotkey for every language.
(Neovim is smart enough to detect and use Hunspell's dictionaries automatically.)

#### Keymaps

I have set two sets of convenience keymaps that I think are specifically useful for LaTeX editing.
The first two for quickly inserting an empty line in normal mode, and the last two for backspacing and deleting entire words with the `CTRL` key in insert mode.

```lua
----<neovim-config-folder>/lua/keymaps.lua
-- insert blank line below
vim.api.nvim_set_keymap("n", "<localleader>o", "mzo<Esc>`z", { noremap = true})

-- insert blank line above
vim.api.nvim_set_keymap("n", "<localleader>O", "mzO<Esc>`z", { noremap = true})

-- CTRL-Backspace to delete word ←
vim.api.nvim_set_keymap("i", "<C-BS>", "<C-o>db", { noremap = true})

-- CTRL-Delete to delete word →
vim.api.nvim_set_keymap("i", "<C-Del>", "<C-o>de", { noremap = true})
```

### Fonts
Fonts aren't really a part of Neovim at all, since you set them through your terminal emulator.
However, having the right font is important, because some fonts are just much more readable and comfortable than others.
Also, without the right font, certain things in Neovim won't show up the way they should – specifically 𝕌𝔫ï̤©𝓸ᵈ🄴 characters and bold, italic, and bold italic text.
Currently, [Hack](https://github.com/source-foundry/Hack) works well for me.
`:h vimtex-syntax-conceal` gives some recommendations too.

## Getting started with LaTeX

Honestly, the best tip I can give any new LaTeX user is to open up [Modern LaTeX](https://github.com/mrkline/modern-latex) and go through chapters 2–4.
That will teach you about installing LaTeX, the basics of writing in LaTeX, and LaTeX document structure better than I ever could.

**→ NOTE:** if you use [Vimtex](#vimtex), you don't have to manually compile your documents with `$ xelatex hello.tex`, you can use `\ll` instead. (though learning it the manual way cannot hurt).

## Useful LaTeX packages

### fontenc

```latex
% Set font encoding to T1
% Comment to restore to LaTeX default (OT1)
% If you have no idea what this is, it's best to leave it
\usepackage[T1]{fontenc}
```

This is really just a commonsense setting to bring LaTeX font encoding into the 21st century.
You can read more about it [here](https://tex.stackexchange.com/questions/664/why-should-i-use-usepackaget1fontenc).

### tgtermes (font)

Fonts are very subjective, but I like Tex Gyre Termes.
If you don't, look for other fonts [here](https://tug.org/FontCatalogue/seriffonts.html) (or hell, just leave it on the default, Computer Modern).

```latex
% I use TeX Gyre Termes because I think it's pretty
% Comment to restore to LaTeX default (Computer Modern)
\usepackage{tgtermes}
```

### fontspec

**→ NOTE:** If you use XeLaTeX of LuaLaTeX instead of the standard pdfLaTeX as your TeX engine, use `fontspec` instead of `fontenc` and the font-specific package.
If you use pdfLaTeX or have no idea what these words even mean, don't load `fontspec`.
Ignore this and move on.

XeLaTeX and LuaLaTeX use `fontspec` in order to load system fonts.
This means that instead of loading a specific font package, you load `fontspec` and specify the font family you want to use.

```latex
\usepackage{fontspec}
\setmainfont{TeX Gyre Termes}
```

### babel (internationalisation)

If you're not writing in American English, `babel` takes care of pretty much all language-related settings for you.
You can give it the language directly by invoking `\usepackage[dutch]{babel}`, but I prefer doing it as follows:

```latex
\documentclass[dutch]{article}

% Sets document language based on the argument passed to the \documentclass variable
\usepackage{babel}
```

This way, any other packages that take a language as an argument also know that your document is in Dutch.

### biblatex-chicago (references and bibliography)

BibLaTeX manages your references and bibliographies.
This particular flavour does so in the [Chicago](https://www.chicagomanualofstyle.org/tools_citationguide.html) style, a popular style in the humanities and social sciences.

→ **NOTE:** If you require a different citation style, you should do some research to see whether you should use plain `biblatex` or a custom version of it.
It seems that for MLA you can use [biblatex-mla](https://ctan.org/pkg/biblatex-mla), and for APA there is [biblatex-apa](https://ctan.org/pkg/biblatex-apa) (both have been updated in 2022), but please do some Ducking before sticking to a method.

In any case, I invoke `biblatex-chicago` as follows (this should work with any biblatex package):

```latex
% Enables BibLaTeX-Chicago in the author-date format, with Biber as backend, and suppressing all doi's and isbn's
% Biber needs to be installed on your machine for this to work!
% Change 'authordate' to 'notes' to use footnotes instead
\usepackage[
authordate,
backend=biber,
doi=true,
isbn=false,
cmsdate=both]
{biblatex-chicago}

% Sets the bibliography source file
% Takes both absolute and relative paths, so all of the following are valid:
%	/home/name/Documents/article/sources.bib
%	sources.bib
%	../sources/sources.bib
\addbibresource{filename.bib}

% Enables csquotes, which is recommended for BibLaTeX-Chicago
\usepackage[autostyle=true]{csquotes}
```

Here, I tell it to use the author-date style, to use Biber as its back-end, to include DOI's but not ISBN's in the bibliography, and to include the original date of a work if specified (through `tex.origdate: <date>`).
Then, I point the package to my `.bib` file.
And lastly, I enable `csquotes`, which is recommended by BibLaTeX.

**Reference management:** I personally maintain my `.bib` file with with the open-source [Zotero](https://www.zotero.org/).
I use the plugin [Better BibTeX](https://retorque.re/zotero-better-bibtex/) to generate and auto-update the `.bib` file, and the [Zotero Connector](https://www.zotero.org/download/connectors) browser extension to easily import sources. 
Lastly, there's the [Zotero Scihub](https://github.com/ethanwillis/zotero-scihub) plugin which automatically downloads pdfs from [Sci-Hub](https://sci-hub.ru/) based on DOIs (which I would never recommend, of course, because it deprives Elsevier of their precious paywall money).

### hyperref (hyperlinks)

Hyperref enables hyperlinks in your documents.

```latex
\usepackage{hyperref}
% Set custom colors for urls, links and citations
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = red, %Colour of internal links
  citecolor   = blue %Colour of citations
}

% The 'caption' package enables the [hypcap=true] option (enabled by default), which lets hyperref refer to the figure itself instead of the caption
\usepackage{caption}
```

The only configuration I do here is some visual tweaks, because the default look of links is… not great.
The `caption` package simply fixes an issue where clicking a link to a figure jumps the pdf to the caption instead of the figure itself.

### cleveref

Cleveref is a way to make internal references a bit easier.
In particular, Cleveref 'knows' what kind of thing you're referring to (a figure, a section, a page), so that it's easier to fit internal references into a sentence.

```latex
% Enables Cleveref, which partly automates internal references
% Instead of 'see paragraph \ref{sec:analysis}', write 'see \cref{sec:analysis}'
% Cleveref will automatically append the type of thing you're referencing (e.g. paragraph, figure, appendix, etc.) and do so in the right language
\usepackage{cleveref}
```

### booktabs (tables)

Booktabs makes it easy to create pretty and clear tables.
Specifically, the package follows [a set of rules](https://ftp.snt.utwente.nl/pub/software/tex/macros/latex/contrib/booktabs/booktabs.pdf) for making what they consider 'good tables'.
I agree with them, so I use it.

```latex
% Making tables in LaTeX can be done without any extra packages
% However, booktabs makes tables simple and pretty
\usepackage{booktabs}
```

You can then make a table like this:

```latex
% By default, this table is put in a floating figure.
% (For more info on figures, see the `image' snippet)
% To put this table in-line instead, comment the lines \begin{figure}, \caption, \label and \end{figure}
\begingroup % A group is created to quarantine the distance parameters set below
% The two lines below increase the distance between columns (\tabcolsep) and between rows (\arraystretch).
\setlength{\tabcolsep}{9pt} % Default LaTeX value: 6pt
\renewcommand{\arraystretch}{1.5} % Default LaTeX value: 1
\begin{figure}
% This table has two paragraphs that wrap their text automatically (hence the p's).
% Besides 'p', other options include c (center), l (left align) and r (right align).
\begin{tabular}{p{0.45\textwidth} p{0.45\textwidth}} \toprule
	\textbf{Header 1} & \textbf{Header 2} \\
	\midrule
	Some text on the left. & Some more text on the right. \\
	This is a somewhat longer though not unreasonably long piece of text. & This is short. \\
	Etc. etc. & You probably get the point by now. \\
		\bottomrule
\end{tabular}
\caption{A very simple example table.}
\label{fig:table}
\end{figure}
\endgroup
```

### pdfpages (external PDF's)

pdfpages lets you include (parts of) PDF files in your document.

```latex
% Insert PDF's
\usepackage{pdfpages}
```

Insert a file like this:

```latex
% The file will be automatically be given its own page, no need to do a \pagebreak before or after
\begin{figure}[h]
    \centering
% If you only want to include certain pages from a document, write them in the first set of curly braces
% Write the filename without extension in the second set of curly brackets
    \includepdf[pages={-}]{frontpage}
\end{figure}
```

### microtype

Microtype makes a bunch of small typesetting changes that make a small but perceptible difference in how your document looks.
It is designed to work its magic out-of-the-box, and newbies (like me) are best off leaving it on its default behaviour.

```latex
% Microtype makes a bunch of small typesetting changes to make your document look better
% Comment to speed up compiling, or to see what the differences are
\usepackage{microtype}
```

### geometry (paper size)

Set the dimensions of your paper.
I personally am too European to know what 'letter paper' even is, so I set mine to A4.

```latex
% Options: a4paper, letterpaper (LaTeX default)
\usepackage[a4paper]{geometry}
```

### setspace (spacing)

We all know that double spacing looks terrible, but sometimes an assignment requires you to use it.
Setspace has you covered.

```latex
\usepackage{setspace}
% Options: singlespacing (default), onehalfspacing, doublespacing
\onehalfspacing
```

### comment

This is just a convenience package, allowing you to comment out large portions of text with the `comment` environment.
Bonus: it works perfectly with [VimTeX](#vimtex), which will correctly highlight comment blocks and ignore them when counting words!

```latex
% Enables comment blocks
\usepackage{comment}

\begin{comment}
This is all commented out.
And so is this!
\end{comment}
```

## LaTeX tips & tricks

### A properly formatted table of contents

If you simply plop down your table of contents, you might have a number of problems:

1) it's too widely or narrowly spaced;
2) it's completely red because of hyperref;
3) it includes all the subsubsections and is now three pages long.

Let's solve all these problems at once:

```latex
{ 
\onehalfspacing
\hypersetup{linkcolor=black} %To make the links in the ToC black instead of red
\setcounter{tocdepth}{2} % To include only sections (level 1) and subsections (level 2) in the ToC
\tableofcontents
}
```

### Abstract and keywords

LaTeX has a separate environment for abstracts, creatively named `abstract`.
Keywords can be formatted manually.

```latex
\begin{abstract}
Bla.
\end{abstract}

\textit{\textbf{\small Keywords --- }\small first, second, third, etc.}
```

### Subtitles

Make a subtitle by making the title bold and adding a line break.

```latex
\title{\textbf{A neat title}\\A neat subtitle}
```

### Subheaders (and table of contents)

If you want to use the above trick with section headers, you run into the problem of having line breaks in your table of contents (since the table of contents literally copies the entire header).
The solution is to provide a separate ToC header in square brackets.
For example:

```latex
\section[Instrumental reason: Horkheimer's Eclipse of Reason]{Instrumental reason\\ {\large Horkheimer's Eclipse of Reason}}
```

### Appendix

When adding an appendix, you probably want a number of things:

1. Starting on a new page;
2. Resetting page numbering;
3. A different page numbering style;
4. Resetting section numbering;
5. A different section numbering style:
6. A separate bibliography with a custom title and a smaller header;
7. A proper place in the table of contents.

Fortunately, the macro `\appendix` takes care of all points except 3 and 6.

The page numbering style can be changed with the `\pagenumbering` command, and the separate bibliography can be arranged with the `refsection` environment.
The custom title and header can be defined after the `\printbibliography` command.

This is what it looks like:

```latex
% This adds an appendix to the document which is separate from the rest of the document, with a separate bibliography.

% Enables separate numbering for the appendix
% Options: arabic, roman, Roman, alph, Alph
\pagenumbering{roman}

% Creates a separate section of references for the appendix bibliography
\begin{refsection}

\appendix

% Write your body text here as you normally would, including chapters, sections, subsections, etc.
\section{First appendix}

% Prints the appendix bibliography with a smaller heading and custom title
\printbibliography[heading=subbibliography,title=Bibliography Appendix A]

\end{refsection}
```

### Every section on new page

Putting every section on its own page automatically requires defining a new command:

```latex
% Start a new page before every section
\let\stdsection\section
\renewcommand\section{\clearpage\stdsection}
```

### Multiple languages

If you're using multiple languages in one document, step 1 is to tell `babel`:

```latex
\usepackage[main=dutch,english]{babel}
```

Then, whenever you need to switch to your secondary language, use the `otherlanguage` environment:

```latex
Stroofwafels zijn slecht voor je.

\begin{otherlanguage}{english}
	``To be, or not to be, that is the question''
\end{otherlanguage}

Deze tekst slaat nergens op.
```

This way, your spell/grammar checker won't freak out, and hyphenation will be correct.

### Two columns

Switching to two columns is as easy as typing `\twocolumn`.
However, in a document which consists fully or mostly of two columns, you should add the option `twocolumn` to the `\documentclass` command (see [this thread](https://tex.stackexchange.com/questions/332120/what-is-the-difference-between-twocolumn-and-documentclasstwocolumnbook) for some reasons).

Apart from that, I slightly tweak the default margins based on my own preferences.

```latex
%%%%%%%%%%%%
% Preamble %
%%%%%%%%%%%%

\documentclass[twocolumn]{article}

%%% Margins %%%
% The default column separation is too small for my taste, so I slightly increase it here
% Comment the three commands below to revert the margins to their LaTeX defaults
% If you're curious about what these commands do to the layout exactly, add \addpackage{layout} to the preamble, and \layout to the main text to see a visual representation

% Increase the column separation by 10pt
\addtolength{\columnsep}{10pt}
% Compensate by pushing the whole text 5pt left
\addtolength{\hoffset}{-5pt}
% Compensate by adding 10pt to the total text width, effectively pushing both columns 5pt outward
\addtolength{\textwidth}{10pt}
```

Switch to one-column mode with `\onecolumn`, and back with `\twocolumn`.

#### Full-width figure in two-column mode
If you insert a regular figure while in two-column mode, it will only span a single column.
For a figure to take up the full two columns, simply add an asterisk, like so:

```latex
\begin{figure*}[h]
	\begin{center}
		% You can specify the size of the image by setting 'width=' or 'height='
		% The size can be set absolutely (in px, cm, etc.), relatively (with 'scale=1.5') or relative to \textwidth, like done below
		\includegraphics[width=0.8\textwidth]{image.png}
		\caption{A very beautiful image.}
		\label{fig:beautiful}
	\end{center}
\end{figure*}
```

### Specific typesetting stuff

#### dashes
`--` makes an en dash (–).

`---` makes an em dash (—).

#### non-breaking spaces
`~` makes a non-breaking space.
Use it before an en dash because you don't want these bad boys ending up on the beginning of a line (`~--`), or between a number and its unit (`10~kg`)

#### ellipses
`\dots` makes an ellipse (…).

#### non-breaking hyphens
`\babelhyphen{nobreak}` makes a non-breaking hyphen.
I've used this once in my whole academic career.

### pdfLaTeX vs. XeLaTeX vs. LuaLaTeX
XeLaTeX and LuaLaTeX are newer TeX engines compared to the much older pdfLaTeX, which is still the de facto standard.

For factual comparisons, see [texfaq](https://texfaq.org/FAQ-xetex-luatex) and appendix A of [Modern LaTeX](https://github.com/mrkline/modern-latex).
For discussions, see [this](https://tex.stackexchange.com/questions/605137/in-2021-does-anything-beat-pdflatex), [this](https://old.reddit.com/r/LaTeX/comments/cj0j8h/pdflatex_or_xelatex/) and [this](https://old.reddit.com/r/LaTeX/comments/x9acv3/which_would_you_recommend_xelatex_or_lualatex/).

As you can see, opinions are divided, and recent in-depth discussions are few and far between.

As for myself, using LuaLaTeX feels like future-proofing my workflow, and its support for system fonts is a nice plus (though XeLaTeX does this too).

---

## Further resources

### LaTeX
- [Modern LaTeX](https://github.com/mrkline/modern-latex) – If you're looking for a thorough yet no-nonsense guide to *actually using* LaTeX, this is probably the guide you're looking for.
- [Overleaf's 30-minute introduction](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes) – Concise walkthrough for beginners.
- [Blog post: LaTeX for the humanities](https://www.overleaf.com/blog/636-guest-blog-post-latex-for-the-humanities) – Excellent writeup on why to use LaTeX in the humanities.
- [LaTeX for Philosophers](https://tanksley.me/latex-for-philosophers/index) – *Very* thorough LaTeX tutorial geared towards philosophy and the humanities in general. (disclaimer: I still need to go through this one myself.)
- [Getting to Grips with LaTeX](https://www.andy-roberts.net/writing/latex) – 12-step LaTeX walkthrough that starts at the very beginning. Great for those who are new to LaTeX!
- [LaTeX-doc-ptr](https://mirrors.evoluso.com/CTAN/info/latex-doc-ptr/latex-doc-ptr.pdf) – A great list of general LaTeX recommendations.

### (neo)vim
- [Vim user manual](https://www.vi-improved.org/vimusermanual.pdf) – The official user-oriented Vim guide. Surprisingly readable and surprisingly useful!
- [Idiomatic vimrc](https://github.com/romainl/idiomatic-vimrc) – A very sensible guide to setting up your own vim config.
- [/u/kaisunc's Vim cheatsheet](https://old.reddit.com/r/vim/comments/n6qfu2/update_my_vim_cheatsheet_static_printable/) – A lovely visual Vim cheatsheet which covers most basic commands.
- [rtorr's Vim cheatsheet](https://vim.rtorr.com/) – A much more comprehensive cheatsheet.
- `vimtutor` – A built-in interactive getting-started guide to Vim. Type `vimtutor` in a terminal to open.
- [Openvim](https://openvim.com/) – An interactive Vim tutorial that covers the basics, much like vimtutor.
- `:h` – Vim's help files are a massive treasure trove of information. Want to know more about syntax highlighting? Type `:h syntax-highlighting` and you're off.
